package gregrorio.zambrano.facci.com.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String URL_PROFESORES = "http://10.22.17.94:3003/profesores/";
    TextView nombre,apellido,titular,id,materia,profesion,cedula;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ObtenerProfesores();
    }
    private void ObtenerProfesores() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PROFESORES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                nombre=(TextView)findViewById(R.id.nombre);
                apellido=(TextView)findViewById(R.id.aplellido);
                titular=(TextView)findViewById(R.id.titular);
                id=(TextView)findViewById(R.id.id);
                materia=(TextView)findViewById(R.id.materia);
                profesion=(TextView)findViewById(R.id.profesion);
              cedula=(TextView)findViewById(R.id.cedula);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject =jsonArray.getJSONObject(i);


                        id.setText(jsonObject.getString("id"));
                        profesion.setText(jsonObject.getString("profesion"));
                        cedula.setText(jsonObject.getString("cedula"));
                        titular.setText(jsonObject.getString("titular"));

                        apellido.setText(jsonObject.getString("apellido"));

                        materia.setText(jsonObject.getString("materia"));
                        nombre.setText(jsonObject.getString("nombre"));
                        //profesores.setImagen(jsonObject.getString("imagen"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
